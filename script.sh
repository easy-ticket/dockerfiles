REGISTRY_NGINX_URL="607958351501.dkr.ecr.us-east-1.amazonaws.com/nginx"

echo "-------------------------------------------------------------------------CHILLOUT"
DOCKERLOGIN=$(aws ecr get-login --region us-east-1 | sed 's/-e none//')
$DOCKERLOGIN

TAGN=${REGISTRY_NGINX_URL}':fargate'
docker build -f base-nginx-fargate/Dockerfile -t base-nginx-fargate base-nginx-fargate/
docker tag base-nginx-fargate ${TAGN}
docker push ${TAGN}
echo ${TAGN}
