# Dockerfiles


## cpp dockerfile

1. Create docker image.
```bash
docker build -t cpp_image .
```

2. Create docker container.
```bash
docker create --name cpp_container -t -i cpp_image bash
```

3. Start container
```bash
docker start -i cpp_container
```
